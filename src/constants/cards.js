import card1 from '../images/card_1.svg'
import card2 from '../images/card_2.svg'
import card3 from '../images/card_3.svg'

export const FOOD = [
    {
        image: card1,
        title: 'Устрицы по рокфеллеровски',
        price: 1300,
        id: 1,
        description: 'Значимость этих проблем настолько очевидна, что укрепление и развитие структуры',
        pice: '750 г.',
    },
    {
        image: card2,
        title: 'Battlefield 2042',
        price: 900,
        id: 2,
        description: 'Не следует, однако забывать, что реализация намеченных плановых',
        pice: '7 шт.'
    },
    {
        image: card3,
        title: 'Life is Strange True Colors',
        price: 750,
        id: 3,
        description: 'Не следует, однако забывать, что реализация намеченных плановых',
        pice: '7 шт.'
    },
    {
        image: card1,
        title: 'Grand Theft Auto V',
        price: 300,
        id: 4,
        description: 'Значимость этих проблем настолько очевидна, что укрепление и развитие структуры ',
        pice: '7 шт.'
    },
    {
        image: card2,
        title: 'Tom Clancy\'s Rainbow Six® Siege',
        price: 2100,
        id: 5,
        description: 'Не следует, однако забывать, что реализация намеченных плановых',
        pice: '7 шт.'
    },
    {
        image: card3,
        title: 'Assassin’s Creed Valhalla',
        price: 2100,
        id: 6,
        description: 'Не следует, однако забывать, что реализация намеченных плановых',
        pice: '7 шт.'
    },
]